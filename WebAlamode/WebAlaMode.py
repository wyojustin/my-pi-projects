#!/usr/bin/env python
#
# Copyright 2009 Facebook
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
"""Simplified chat demo for websockets.

Authentication, error handling, etc are left as an exercise for the reader :)
"""
print "Begin ..."
import logging
import tornado.escape
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.websocket
import os.path
import uuid
import pyfirmata
from pyfirmata import util

from tornado.options import define, options

define("port", default=8888, help="run on the given port", type=int)


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/", MainHandler),
            (r"/digi/value/([0-9]+)/(.*)", DigiValueHandler),
            (r"/digi/mode/([0-9]+)/(.*)", DigiModeHandler),
            (r"/analog/mode/([0-9]+)/(.*)", AnalogModeHandler),
            (r"/ws", WSHandler),
        ]
        settings = dict(
            cookie_secret="__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__",
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            xsrf_cookies=True,
            autoescape=None,
            ssl_options={ 
"certfile": os.path.join(os.path.join(os.path.dirname(__file__), "data"), "mydomain.crt"), 
"keyfile": os.path.join(os.path.join(os.path.dirname(__file__), "data"), "mydomain.key"), }
        )
        tornado.web.Application.__init__(self, handlers, **settings)

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("form.html")

BOOLS = {'on':True,
         'off':False}
PORT = '/dev/ttyS0'

# Creates a new board 
alamode = pyfirmata.Arduino(PORT, baudrate=57600)
for dp in alamode.digital_ports:
    dp.enable_reporting()
it = util.Iterator(alamode)
it.start()

class MyPin:
    __pins = {}
    def __init__(self, pin, val=False):
        self.pin = pin
        # self.pin.write(val) ## default to known state
        self.pin.mode = pyfirmata.INPUT
        self.last = self.pin.read()
        self.callbacks = []
        MyPin.__pins[pin] = self
    def _get_mode(self):
        return self.pin.mode
    def _set_mode(self, io):
        self.pin.mode = io
    mode = property(_get_mode, _set_mode)
    def add_callback(self, callback):
        '''
        callback functions take on argument pin
        called on pin change
        '''
        self.callbacks.append(callback)
    def remove_callback(self, cb):
        try:
            self.callbacks.remove(cb)
        except:
            pass
    def onChange(self):
        for cb in self.callbacks:
            cb(self.pin)

    @classmethod
    def query(kls):
        for pin in kls.__pins:
            v = pin.read()
            mypin = kls.__pins[pin]
            if v != mypin.last:
                mypin.last = v
                mypin.onChange()
    

# for pin in alamode.digital[2:14]:
    # p = MyPin(pin, False)
    # p.add_callback(hi)

# pin_polling = tornado.ioloop.PeriodicCallback(MyPin.query, 100, io_loop=None)
# pin_polling.start()
    
class DigiValueHandler(tornado.web.RequestHandler):
    def get(self, pin, val):
        try:
            pin = int(pin)
            val = BOOLS[val]
            # print pin, val
            alamode.digital[pin].mode = pyfirmata.OUTPUT
            alamode.digital[pin].write(val)
        except Exception, v:
            print v
            pass 
class DigiModeHandler(tornado.web.RequestHandler):
    notify_list = []
    def get(self, pin_id, val):
        try:
            pin_id = int(pin_id)
            pin = alamode.digital[pin_id]
            if val == 'input':
                pin.mode = pyfirmata.INPUT
            else:
                pin.mode = pyfirmata.OUTPUT
            self.notify(pin)
            # print pin, val
        except Exception, v:
            print v
            raise
    @classmethod
    def register(kls, callback):
        kls.notify_list.append(callback)
    @classmethod
    def unregister(kls, callback):
        kls.notify_list.remove(callback)
    def notify(self, pin):
        '''
        notify all listeners to mode change on this pin (firmata pin object)
        '''
        for cb in self.notify_list:
            cb(pin)
            
class AnalogModeHandler(tornado.web.RequestHandler):
    notify_list = []
    def get(self, pin_id, mode):
    	'''
    	called when mode is change between "sample" or "off"
    	'''
        try:
            pin_id = int(pin_id)
            pin = alamode.analog[pin_id]
            pin.mode = pyfirmata.ANALOG;
            if mode == 'sample':
                pin.mode = pyfirmata.INPUT
                self.notify(pin)
                pin.enable_reporting()
            else:
                pin.mode = pyfirmata.OUTPUT
                pin.disable_reporting()
            # print pin, val
        except Exception, v:
            print v
            raise
    @classmethod
    def register(kls, callback):
        kls.notify_list.append(callback)
    @classmethod
    def unregister(kls, callback):
        kls.notify_list.remove(callback)
    def notify(self, pin):
        '''
        notify all listeners to mode change on this pin (firmata pin object)
        '''
        for cb in self.notify_list:
            cb(pin)
            
class WSHandler(tornado.websocket.WebSocketHandler):
    def allow_draft76(self):
        # for iOS 5.0 Safari
        return True
    def open(self):
        print 'new connection'
        self.write_message("Hello World")
        # for i in range(3):
        #     self.write_message('Testing %d' % i)
        ## register for pin change callbacks
        for pin in alamode.digital[2:14]:
            pin.register(self.pin_update)
        for pin in alamode.analog[0:5]:
            pin.register(self.pin_update)
        DigiModeHandler.register(self.pin_update)
        AnalogModeHandler.register(self.pin_update)
    def pin_update(self, pin):
    	if pin.type == pyfirmata.DIGITAL:
        	self.write_message('digi_value_%s %s' % (pin.pin_number, str(pin.read()).lower()))
    	elif pin.type == pyfirmata.ANALOG:
        	self.write_message('analog_value_%s %s' % (pin.pin_number, str(pin.read()).lower()))
        
    def on_message(self, message):
        print 'message received %s' % message

    def on_close(self):
        print 'connection closed'
        for pin in alamode.digital[2:14]:
            # mypin = MyPin(pin)
            pin.unregister(self.pin_update)
        DigiModeHandler.unregister(self.pin_update)
        AnalogModeHandler.unregister(self.pin_update)
      

def main():
    tornado.options.parse_command_line()
    app = Application()
    app.listen(options.port)
    print 'serving on port', options.port
    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    main()
