// Based on FriendFeed
// Author: Justin Shaw
// email:justin@wyolum.com
// Company: WyoLum

$(window).load(function() {
    $("img").each(function() {this.width("200px");});
});

function log(m) {
  d = document.getElementById("log");
  d.innerHTML = m + "<br/>"; //+ d.innerHTML;
}

$(document).ready(function() {
$( "#progressbar" ).progressbar({
            value: 100
});
    var ws = new WebSocket("ws://raspberrypi.local:8888/ws");
    
    ws.onopen = function(evt) { log("socket opened");
            $("#form_container").css("background-color", "#eeeeee");
    };
    ws.onmessage = function(evt) { 
      log("message: " + evt.data.split(" ")); 
      $("input[name='" + evt.data.split(" ")[0] + "']").attr('checked', 
                    evt.data.split(" ")[1] == "true");
      $("input[name='" + evt.data.split(" ")[0] + "']").attr('value', 
                    evt.data.split(" ")[1]);
    // ws.send("GOTIT");
    };
    ws.onclose = function(evt) { 
        log("socket closed");
        $("#form_container").css("background-color", "#444444");
    };
    
    if (!window.console) window.console = {};
    if (!window.console.log) window.console.log = function() {};
    $("input:radio").click(function(){
      if(this.name.search("digi") >= 0){
        $.get("/digi/" + this.value);
      }
      if(this.name.search("analog") >= 0){
        $.get("/analog/" + this.value);
      }
    });
    $("input:checkbox").click(function(){
      // name is digi_value_## 
      // update digi_mode_##
      var pin_number = this.name.split("_")[2];
      //$("#digi_output_2").attr("checked", true);
      $("#digi_output_" + pin_number).attr("checked", true);
      if(this.checked){
        $.get("/digi/value/" + pin_number + '/on');
      }
      else{
        $.get("/digi/value/" + pin_number + '/off');
      }
    });
    $("#messageform").live("submit", function() {
        newMessage($(this));
        return false;
    });
    $("#messageform").live("keypress", function(e) {
        if (e.keyCode == 13) {
            newMessage($(this));
            return false;
        }
    });
    $("#message").select();
    updater.start();
});
 
