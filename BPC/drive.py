import time
import sys
import struct
from serial import *

port = '/dev/ttyUSB0' # laptops
port = '/dev/ttyS0' # pi
baudrate = 115200
timeout = .1
s = Serial(port, baudrate, timeout=timeout)
s.flush()
# s.rtscts = True

SER_HEADER = chr(0)
def send_msg(init=False, interval_ms=0, pump_rate=0, valve=0):
    if init:
        init = 1
    msg = SER_HEADER + chr(init) + chr(interval_ms) + chr(pump_rate) + chr(valve)
    s.write(msg)
    time.sleep(.1)

def __test__():
    send_msg(init=True);
    send_msg(init=False, interval_ms=50, pump_rate=1, valve=255)
    print s.read(1000)
    time.sleep(5)

    send_msg(interval_ms=0)
    sys.stdout.write(s.read(100))
    s.flush()
__test__()