###
# Servo control for ALAMODE!
# WyoLum 2012
# Justin Shaw
import time
from Adafruit_I2C import Adafruit_I2C

N_SERVO = 6

alamode = Adafruit_I2C(42);

class Servo:
    __msg__ = [0] * (3 * N_SERVO)
    def __init__(self, channel, pos=0):
        '''
        channel -- what channel is the servo on?
        position -- initial position of the servo
        
        Channel should be 0,1,2,3,4, or 5
        '''
        self.chan = channel
        self.attach()
        # self.setPos(pos)
    def attach(self):
        alamode.write8(self.chan, True) #  attach
    def detach(self):
        alamode.write8(self.chan, False) # detach    
    def setPos(self, pos):
        self.pos = pos
        alamode.writeS16(N_SERVO + 2 * self.chan, self.pos)
servos = []
angles = [45, 90]
for i in range(0, N_SERVO, 3):
    servos.append(Servo(i))
    time.sleep(.1)
for i in range(2):
    for angle in angles:
        print angle
        for servo in servos:
            servo.setPos(angle)
        time.sleep(1)
for servo in servos:
  servo.detach()
