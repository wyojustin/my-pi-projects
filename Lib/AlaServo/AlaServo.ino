#include <stdint.h>
#include <Wire.h>
#include <Servo.h>
template<class T> inline Print &operator <<(Print &obj, T arg){obj.print(arg); return obj;} 

// each register is three bytes
// bytes0-6 (boolean) -- status (0 -- off, 1 -- enabled)
// bytes6-18 (short) -- angles in degrees

const int WID = 42;
const byte N_SERVO = 6;
const int N_DATA_BYTE = 3 * N_SERVO;
uint8_t data[N_DATA_BYTE];
uint8_t address;
const int BAUD = 9600;
int pwm_pins[6] = {3, 5, 6, 9, 10, 12};
boolean new_data = false;

Servo servos[6];

void setup(){
  Serial.begin(BAUD);
  Wire.begin(WID);

  Wire.onReceive(ALAMODE_onReceive);
  Wire.onRequest(ALAMODE_onRequest);
  for(int ii=0; ii < N_DATA_BYTE; ii++){
    data[ii] = 0;
  }
  servos[0].attach(pwm_pins[0]);
  /*
  while(true){
    servos[0].write(90);
    delay(500);
    servos[0].write(45);
    delay(500);
  }*/
Serial.println("AlaMode Servo Controller");
}

void loop(){
  short pos;
  while(Serial.available()){
    Serial.write(Serial.read()); // ECHO SERIAL
  }
  if(new_data){
    for(int ii=0; ii < 3 * N_SERVO; ii++){
      Serial.print(data[ii], DEC);
      Serial.print(" ");
    }
   Serial.println("");
    for(int ii=0; ii < N_SERVO; ii++){
      if(data[ii]){ // servo is enabled?
        pos = (data[N_SERVO + 2 * ii]) + (data[N_SERVO + 2 * ii + 1] << 8);
        // pos = *(data + N_SERVO + 2 * ii);
        servos[ii].write(pos);
        Serial << "Servo[" << ii << "].write( " << pos << ");" << "\n";
      }
   new_data = false;
  }
  }
  delay(100);
}

void ALAMODE_onReceive(int n_byte){
  new_data = true;
  address = Wire.read();
  int idx = address;
  while(Wire.available() && (idx < N_DATA_BYTE)){
    data[idx] = Wire.read();
    if(idx < N_SERVO){
      if(data[idx]){ // enable servo
        servos[idx].attach(pwm_pins[idx]);
      }
      else{ // disable servo
        servos[idx].detach();
      }
    }
    idx++;
  }
}

void ALAMODE_onRequest(){
  int n_byte = 32;
  if(N_DATA_BYTE - address < 32){
    n_byte = N_DATA_BYTE - address;
  }
  Wire.write(data + address, n_byte);
}

